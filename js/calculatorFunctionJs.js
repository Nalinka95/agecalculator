function populate(currentYear, currentMonth, currentDay) {
    const yearDoc = document.getElementById(currentYear);
    const year = yearDoc.options[yearDoc.selectedIndex].value;
    const month = document.getElementById(currentMonth);
    const day = document.getElementById(currentDay);
    day.innerHTML = "";
    if (month.value == "February") {
        const reminderOfYear = year % 4;
        if (reminderOfYear == 0) {
            initializingDays(currentDay, 29);
        } else {
            initializingDays(currentDay, 28);
        }
    } else if (["April", "June", "September", "November"].indexOf(month.value) >= 0) {
        initializingDays(currentDay, 30);
    } else {
        initializingDays(currentDay, 31);
    }
}

function initializeAll(currentYear, currentMonth, currentDay) {
    initializingYears(currentYear);
    initializingMonth(currentMonth);
    initializingDays(currentDay, 31);
}

function initializingYears(presentYear) {

    const today = new Date();
    const yearOfToday = today.getFullYear();

    const currentYear = document.getElementById(presentYear);
    currentYear.innerHTML = "";
    let yearArray = ["Y"];
    let index = 1;
    let year;
    for (year = yearOfToday; year > yearOfToday - 140; year--) {
        yearArray[index] = year;
        index++;
    }
    for (index = 0; index < yearArray.length; index++) {
        const newOption = document.createElement("option");
        newOption.value = yearArray[index];
        newOption.innerHTML = yearArray[index];
        currentYear.options.add(newOption);
    }
}

function initializingMonth(month) {
    const currentMonth = document.getElementById(month);
    currentMonth.innerHTML = "";
    const monthArray = ["M", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    let index = 0;
    for (index = 0; index < monthArray.length; index++) {
        const newOption = document.createElement("option");
        newOption.value = monthArray[index];
        newOption.innerHTML = monthArray[index];
        currentMonth.options.add(newOption);
    }
}

function initializingDays(currentDate, maxDay) {
    const currentDay = document.getElementById(currentDate);
    currentDay.innerHTML = "";
    let dayArray = ["D"];
    let day;
    for (day = 1; day < maxDay + 1; day++) {
        dayArray[day] = day;
    }

    let index;
    for (index = 0; index < dayArray.length; index++) {
        const newOption = document.createElement("option");
        newOption.value = dayArray[index];
        newOption.innerHTML = dayArray[index];
        currentDay.options.add(newOption);
    }
}

function isValidate(name, year, month, day) {
    if (!isDateValidate(year, month, day) && !isNameValidate(name)) {
        displayError("Please fill birthday and name correctly.");
        return false;
    } else if (!isDateValidate(year, month, day)) {
        displayError("Please fill birthday correctly.");
        return false;
    } else if (!isNameValidate(name)) {
        displayError("Please fill name correctly.");
        return false;
    } else {
        return true;
    }
}

function isNameValidate(name) {
    const letters = /^[A-Za-z]+$/;
    if (name.match(letters)) {
        return true;
    } else {
        return false;
    }
}

function isDateValidate(year, month, day) {
    if (isNaN(year) || month == -1 || isNaN(day)) {
        return false;
    } else {
        return true;
    }
}

function calculateAge() {
    const monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    document.getElementById("error").innerHTML = "";
    document.getElementById("result").innerHTML = "";

    const yearDoc = document.getElementById("yearList");
    const customerYear = parseInt(yearDoc.options[yearDoc.selectedIndex].value);

    const monthDoc = document.getElementById("monthList");
    const customerMonthInString = monthDoc.options[monthDoc.selectedIndex].value;
    const customerMonth = monthArray.indexOf(customerMonthInString) + 1;

    const dayDoc = document.getElementById("dayList");
    const customerDay = parseInt(dayDoc.options[dayDoc.selectedIndex].value);
    const today = new Date();
    const yearOfToday = today.getFullYear();
    const monthOfToday = today.getMonth() + 1;
    const dateOfToday = today.getDate();

    const name = document.getElementById("name").value.toString();

    if (isValidate(name, customerYear, customerMonth, customerDay)) {
        if (yearOfToday == customerYear) {
            if (monthOfToday < customerMonth) {
                displayError("The entered date is a future date");
            } else if (monthOfToday == customerMonth) {
                if (dateOfToday < customerDay) {
                    displayError("The entered date is a future date");
                } else {
                    checkDayConditions(name, 0, 0, dateOfToday, customerDay);
                }
            } else {
                const monthDifference = monthOfToday - customerMonth;
                checkDayAndMonthConditions(name, 0, monthDifference, customerDay, dateOfToday, customerMonth, yearOfToday);
            }
        } else if (yearOfToday > customerYear) {
            const yearDifference = yearOfToday - customerYear;
            if (monthOfToday < customerMonth) {
                const monthDifference = monthOfToday - customerMonth;
                checkDayAndMonthConditions(name, yearDifference - 1, 12 + monthDifference, customerDay, dateOfToday, customerMonth, yearOfToday);
            } else if (monthOfToday == customerMonth) {
                checkDayAndMonthConditions(name, yearDifference, 0, customerDay, dateOfToday, customerMonth, yearOfToday);
            } else {
                const monthDifference = monthOfToday - customerMonth;
                checkDayAndMonthConditions(name, yearDifference, monthDifference, customerDay, dateOfToday, customerMonth, yearOfToday);
            }

        }

    }
}

function displayResult(name, year, month, day) {
    if (year == 0 && month == 0 && day == 0) {
        document.getElementById("result").innerHTML = `Today is ${name}\'s birthday.`;
    } else if (year == 0 && month == 0) {
        document.getElementById("result").innerHTML = `${name} is ${day} days old.`
    } else if (year == 0 && day == 0) {
        document.getElementById("result").innerHTML = `${name} is ${month} months old.`
    } else if (day == 0 && month == 0) {
        document.getElementById("result").innerHTML = `${name} is ${year} years old.`
    } else if (year == 0) {
        document.getElementById("result").innerHTML = `${name} is ${month} months and ${day} days old.`
    } else if (month == 0) {
        document.getElementById("result").innerHTML = `${name} is ${year} years and ${day} days old.`
    } else if (day == 0) {
        document.getElementById("result").innerHTML = `${name} is ${year} years and ${month} months old.`
    } else {
        document.getElementById("result").innerHTML = `${name} is ${year} years, ${month} months and ${day} days years old`;
    }

}

function displayError(errorMsg) {
    document.getElementById("error").innerHTML = errorMsg;
}

function checkMonthConditions(name, yearDifference, MonthDifference, dayDifference, customerMonth, yearOfToday) {
    const monthIsOdd = customerMonth % 2;
    if (customerMonth == 2 && yearOfToday % 4 == 0) {
        displayResult(name, yearDifference, MonthDifference, 29 - dayDifference);
    } else if (customerMonth == 2) {
        displayResult(name, yearDifference, MonthDifference, 28 - dayDifference);
    } else if (monthIsOdd == 1) {
        displayResult(name, yearDifference, MonthDifference, 31 - dayDifference);
    } else if (monthIsOdd == 0) {
        displayResult(name, yearDifference, MonthDifference, 30 - dayDifference);
    }
}

function checkDayConditions(name, yearDifference, monthDifference, dateOfToday, customerDay) {
    if (dateOfToday == customerDay) {
        displayResult(name, yearDifference, monthDifference, 0);
    } else {
        displayResult(name, yearDifference, monthDifference, dateOfToday - customerDay);
    }
}

function checkDayAndMonthConditions(name, yearDifference, monthDifference, customerDay, dateOfToday, customerMonth, yearOfToday) {
    let monthMinusOne;
    let yearDiff;
    if (monthDifference === 0) {
        monthMinusOne = 11;
        yearDiff = yearDifference - 1;
    } else {
        monthMinusOne = monthDifference - 1;
        yearDiff = yearDifference;
    }
    if (dateOfToday < customerDay) {
        checkMonthConditions(name, yearDiff, monthMinusOne, customerDay - dateOfToday, customerMonth, yearOfToday);
    } else {
        checkDayConditions(name, yearDifference, monthDifference, dateOfToday, customerDay);
    }

}
